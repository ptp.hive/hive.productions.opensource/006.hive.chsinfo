﻿using HiVE.CHSInfo.isClass;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace HiVE.CHSInfo.isForm
{
    public partial class frmStartup : Form
    {
        #region ConnectionTryCatch
        //----------

        private TryCatch _TryCatch = new TryCatch();
        private static string _GetMEC_Forms = string.Empty;
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionTryCatch

        public frmStartup()
        {
            InitializeComponent();
            try
            {
                _GetMEC_Forms = _TryCatch.GetMEC_FormStartup;

                progressBar_Startup.Value = 0;
                lblCompanyLogo.BackgroundImage = HiVE.isResources.res_HiVE.HiVE_Logo_x256;
                lblCompanyName.Text = Application.CompanyName;

                lblProductLogo.BackgroundImage = HiVE.isResources.res_HiVE.logo_x256;
                lblProductName.Text += " " + Application.ProductVersion.Substring(0, 2);

                picCompanyLogo.Image = HiVE.isResources.res_HiVE.HiVE_Company_Logo_x512;
                isRunedPrerequisites = true;
            }
            catch (Exception exc)
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        #region ConnectionModels
        //----------

        private bool isRunedPrerequisites = true;

        //----------
        #endregion ConnectionModels

        #region MultiThreaded
        //----------

        private Thread trPrerequisites;
        private ThreadStart trsPrerequisites;
        private string MesExpErrorForms_trdPrq = string.Empty;

        //----------
        #endregion MultiThreaded

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        private void bgwPrerequisites_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                isRunedPrerequisites = true;

                Invoke(new MethodInvoker(delegate () { Cursor = Cursors.WaitCursor; }));

                //Settings_Main.Default.TrialMode = !_LicenseValidator.GetCheckingLicense;
                //_RunPrerequisites.RuningPrerequisites();

                Invoke(new MethodInvoker(delegate () { Cursor = Cursors.Default; }));
            }
            catch (Exception exc)
            {
                Invoke(new MethodInvoker(delegate () { Cursor = Cursors.Default; }));

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms_trdPrq += _GetMEC_Forms + "0100: " + exc.Message;
                }
                else
                {
                    /// خطا در بارگیری پیش‌نیازهای برنامه
                    MesExpErrorForms_trdPrq += _GetMEC_Forms + "0100: " + "Error loading program requirements. ";
                }

                _TryCatch.GetCEM_Error(MesExpErrorForms_trdPrq);
                MesExpErrorForms_trdPrq = "";

                Invoke(new MethodInvoker(delegate () { this.DialogResult = DialogResult.No; }));
            }
        }

        private void bgwPrerequisites_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                Invoke(new MethodInvoker(delegate () { Cursor = Cursors.WaitCursor; }));
            }
            catch { }
        }

        private void bgwPrerequisites_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                isRunedPrerequisites = false;
            }
            catch { }
        }

        private void timerStartup_Tick(object sender, EventArgs e)
        {
            try
            {
                if (progressBar_Startup.Value < 100)
                {
                    progressBar_Startup.Value += 1;
                }
                else if (!isRunedPrerequisites)
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch { }
        }

        //------
        //------------
        ////----------////---------------------------------------------------------------------// Begin Form_Load
        //------------

        private void frmStartup_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                timerStartup.Enabled = true;

                try
                {
                    trsPrerequisites = new ThreadStart(bgwPrerequisites.RunWorkerAsync);
                    trPrerequisites = new Thread(trsPrerequisites);
                    trPrerequisites.Priority = ThreadPriority.Highest;
                    //
                    trPrerequisites.Start();
                }
                catch { }

                Cursor = Cursors.Default;

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// فرآیند بارگیری برنامه با خطا همراه بود
                    /// مجددا تلاش نمایید
                    MessageError = "Error loading program. ";

                    MesExpErrorForms += _GetMEC_Forms + "0000: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //------------
        ////----------////---------------------------------------------------------------------// End Form_Load
        //------------
    }
}

