﻿using HiVE.CHSInfo.isClass;
using System;
using System.Windows.Forms;

namespace HiVE.CHSInfo.isForm
{
    public partial class frmAbout : Form
    {
        #region ConnectionTryCatch
        //----------

        private TryCatch _TryCatch = new TryCatch();
        private static string _GetMEC_Forms = string.Empty;
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionTryCatch

        public frmAbout()
        {
            InitializeComponent();
            try
            {
                _GetMEC_Forms = _TryCatch.GetMEC_FormAbout;

                picProductLogo.Image = HiVE.isResources.res_HiVE.logo_x256;
                picCompanyLogo.Image = HiVE.isResources.res_HiVE.HiVE_Company_Logo_x256;
            }
            catch (Exception exc)
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //------
        //------------
        ////----------////---------------------------------------------------------------------// Begin Form_Load
        //------------

        private void frmAbout_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                lblProduct_Title.Text = "CheckSum Information";
                lblProduct_Version.Text = "Version " + Application.ProductVersion;
                lblProduct_Description.Text = "Calculating CheckSum Information Files.";
                lblProduct_Type.Text = "THIS SOFTWARE IS FREEWARE";
                lblProduct_MoreDescription.Text = "";

                linkLblCompany_WebSite.Text = "http://www.ptp.hive.com";
                linkLblCompany_Email.Text = "info@ptp.hive.com";

                lblProduct_Copyright.Text = "Copyright ©  2016 HiVE Productions Inc. All Rights Reserved.";

                Cursor = Cursors.Default;

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// فرآیند بارگیری درباره ما با خطا همراه بود
                    /// مجددا تلاش نمایید
                    MessageError = "Error loading about us. ";

                    MesExpErrorForms += _GetMEC_Forms + "0000: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //------------
        ////----------////---------------------------------------------------------------------// End Form_Load
        //------------
    }
}
