﻿namespace HiVE.CHSInfo.isForm
{
    partial class frmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStartup));
            this.tlpFormStartup = new System.Windows.Forms.TableLayoutPanel();
            this.picCompanyLogo = new System.Windows.Forms.PictureBox();
            this.progressBar_Startup = new System.Windows.Forms.ProgressBar();
            this.timerStartup = new System.Windows.Forms.Timer(this.components);
            this.bgwPrerequisites = new System.ComponentModel.BackgroundWorker();
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblProductName = new System.Windows.Forms.ToolStripLabel();
            this.lblCompanyLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblCompanyName = new System.Windows.Forms.ToolStripLabel();
            this.toolStripStartup = new System.Windows.Forms.ToolStrip();
            this.tlpFormStartup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCompanyLogo)).BeginInit();
            this.toolStripStartup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpFormStartup
            // 
            this.tlpFormStartup.BackColor = System.Drawing.Color.Transparent;
            this.tlpFormStartup.ColumnCount = 3;
            this.tlpFormStartup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpFormStartup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFormStartup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlpFormStartup.Controls.Add(this.progressBar_Startup, 1, 3);
            this.tlpFormStartup.Controls.Add(this.toolStripStartup, 1, 1);
            this.tlpFormStartup.Controls.Add(this.picCompanyLogo, 1, 2);
            this.tlpFormStartup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFormStartup.Location = new System.Drawing.Point(0, 0);
            this.tlpFormStartup.Name = "tlpFormStartup";
            this.tlpFormStartup.RowCount = 4;
            this.tlpFormStartup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpFormStartup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tlpFormStartup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFormStartup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tlpFormStartup.Size = new System.Drawing.Size(624, 441);
            this.tlpFormStartup.TabIndex = 17;
            // 
            // picCompanyLogo
            // 
            this.picCompanyLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.picCompanyLogo.BackColor = System.Drawing.Color.Transparent;
            this.picCompanyLogo.Location = new System.Drawing.Point(53, 83);
            this.picCompanyLogo.Name = "picCompanyLogo";
            this.picCompanyLogo.Size = new System.Drawing.Size(518, 299);
            this.picCompanyLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCompanyLogo.TabIndex = 4;
            this.picCompanyLogo.TabStop = false;
            // 
            // progressBar_Startup
            // 
            this.progressBar_Startup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar_Startup.Location = new System.Drawing.Point(53, 404);
            this.progressBar_Startup.Name = "progressBar_Startup";
            this.progressBar_Startup.Size = new System.Drawing.Size(518, 18);
            this.progressBar_Startup.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar_Startup.TabIndex = 5;
            this.progressBar_Startup.Value = 50;
            // 
            // timerStartup
            // 
            this.timerStartup.Enabled = true;
            this.timerStartup.Tick += new System.EventHandler(this.timerStartup_Tick);
            // 
            // bgwPrerequisites
            // 
            this.bgwPrerequisites.WorkerReportsProgress = true;
            this.bgwPrerequisites.WorkerSupportsCancellation = true;
            this.bgwPrerequisites.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwPrerequisites_DoWork);
            this.bgwPrerequisites.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwPrerequisites_ProgressChanged);
            this.bgwPrerequisites.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwPrerequisites_RunWorkerCompleted);
            // 
            // lblProductLogo
            // 
            this.lblProductLogo.AutoSize = false;
            this.lblProductLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.Size = new System.Drawing.Size(56, 56);
            this.lblProductLogo.Text = "Product.Logo";
            // 
            // lblProductName
            // 
            this.lblProductName.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.lblProductName.ForeColor = System.Drawing.Color.Red;
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(179, 57);
            this.lblProductName.Text = "CheckSum Information";
            // 
            // lblCompanyLogo
            // 
            this.lblCompanyLogo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblCompanyLogo.AutoSize = false;
            this.lblCompanyLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblCompanyLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblCompanyLogo.Name = "lblCompanyLogo";
            this.lblCompanyLogo.Size = new System.Drawing.Size(56, 56);
            this.lblCompanyLogo.Text = "Company.Logo";
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblCompanyName.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.lblCompanyName.ForeColor = System.Drawing.Color.DarkOrange;
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(140, 57);
            this.lblCompanyName.Text = "HiVE Productions";
            // 
            // toolStripStartup
            // 
            this.toolStripStartup.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStartup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripStartup.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripStartup.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblProductName,
            this.lblCompanyLogo,
            this.lblCompanyName});
            this.toolStripStartup.Location = new System.Drawing.Point(50, 20);
            this.toolStripStartup.Name = "toolStripStartup";
            this.toolStripStartup.Size = new System.Drawing.Size(524, 60);
            this.toolStripStartup.TabIndex = 7;
            this.toolStripStartup.Text = "toolStrip Startup";
            // 
            // frmStartup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.tlpFormStartup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(640, 480);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "frmStartup";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Startup CheckSum Information";
            this.Load += new System.EventHandler(this.frmStartup_Load);
            this.tlpFormStartup.ResumeLayout(false);
            this.tlpFormStartup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCompanyLogo)).EndInit();
            this.toolStripStartup.ResumeLayout(false);
            this.toolStripStartup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpFormStartup;
        private System.Windows.Forms.PictureBox picCompanyLogo;
        private System.Windows.Forms.ProgressBar progressBar_Startup;
        private System.Windows.Forms.Timer timerStartup;
        private System.ComponentModel.BackgroundWorker bgwPrerequisites;
        private System.Windows.Forms.ToolStrip toolStripStartup;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblProductName;
        private System.Windows.Forms.ToolStripLabel lblCompanyLogo;
        private System.Windows.Forms.ToolStripLabel lblCompanyName;
    }
}