﻿namespace HiVE.CHSInfo.isForm
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.btnMinimized = new System.Windows.Forms.ToolStripButton();
            this.btnAbout = new System.Windows.Forms.ToolStripButton();
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblProduct_Name = new System.Windows.Forms.ToolStripLabel();
            this.panelFormMain = new System.Windows.Forms.Panel();
            this.tlpFormMain = new System.Windows.Forms.TableLayoutPanel();
            this.progressBarStatus_RunFunction = new System.Windows.Forms.ProgressBar();
            this.grbCheckSumType = new System.Windows.Forms.GroupBox();
            this.cmbCheckSumType = new System.Windows.Forms.ComboBox();
            this.grbInputFile = new System.Windows.Forms.GroupBox();
            this.checkBoxReadOnly_ShowInputFilePath = new System.Windows.Forms.CheckBox();
            this.btnLoadFile = new System.Windows.Forms.Button();
            this.txtShowInputFilePath = new System.Windows.Forms.TextBox();
            this.grbComparisonCheckSum = new System.Windows.Forms.GroupBox();
            this.lblResultComparison = new System.Windows.Forms.Label();
            this.btnClear_ComparisonCheckSum = new System.Windows.Forms.Button();
            this.btnCheckingComparison = new System.Windows.Forms.Button();
            this.btnPaste_ComparisonCheckSum = new System.Windows.Forms.Button();
            this.txtComparisonCheckSum = new System.Windows.Forms.TextBox();
            this.grbCheckSumInformation = new System.Windows.Forms.GroupBox();
            this.checkBoxReadOnly_CHSInfo = new System.Windows.Forms.CheckBox();
            this.lblCheckSumType = new System.Windows.Forms.Label();
            this.btnToLower_CheckSum = new System.Windows.Forms.Button();
            this.btnToUpper_CheckSum = new System.Windows.Forms.Button();
            this.btnCopy_CheckSum = new System.Windows.Forms.Button();
            this.txtCheckSumInformation = new System.Windows.Forms.TextBox();
            this.tlpRunResetFunction = new System.Windows.Forms.TableLayoutPanel();
            this.btnResetFunction = new System.Windows.Forms.Button();
            this.btnRunFunction = new System.Windows.Forms.Button();
            this.toolStripStatus = new System.Windows.Forms.ToolStrip();
            this.progressBarStatus_Function = new System.Windows.Forms.ToolStripProgressBar();
            this.lblTimerStatus_Lasted = new System.Windows.Forms.ToolStripLabel();
            this.timerStatus_Function = new System.Windows.Forms.Timer(this.components);
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblStatus_Procces = new System.Windows.Forms.ToolStripLabel();
            this.toolStripMenu.SuspendLayout();
            this.toolStripLogo.SuspendLayout();
            this.panelFormMain.SuspendLayout();
            this.tlpFormMain.SuspendLayout();
            this.grbCheckSumType.SuspendLayout();
            this.grbInputFile.SuspendLayout();
            this.grbComparisonCheckSum.SuspendLayout();
            this.grbCheckSumInformation.SuspendLayout();
            this.tlpRunResetFunction.SuspendLayout();
            this.toolStripStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExit,
            this.btnMinimized,
            this.btnAbout});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripMenu.Size = new System.Drawing.Size(444, 25);
            this.toolStripMenu.TabIndex = 2;
            this.toolStripMenu.Text = "toolStripMenu";
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExit.Image = global::Properties.Resources.btnExit;
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(23, 22);
            this.btnExit.Text = "Exit";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMinimized
            // 
            this.btnMinimized.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMinimized.Image = global::Properties.Resources.btnMinimized;
            this.btnMinimized.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMinimized.Name = "btnMinimized";
            this.btnMinimized.Size = new System.Drawing.Size(23, 22);
            this.btnMinimized.Text = "Minimized";
            this.btnMinimized.Click += new System.EventHandler(this.btnMinimized_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(125, 22);
            this.btnAbout.Text = "About [Product_Title]";
            this.btnAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblProduct_Name});
            this.toolStripLogo.Location = new System.Drawing.Point(0, 25);
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.Size = new System.Drawing.Size(444, 53);
            this.toolStripLogo.TabIndex = 3;
            this.toolStripLogo.Text = "toolStripLogo";
            // 
            // lblProduct_Name
            // 
            this.lblProduct_Name.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.lblProduct_Name.ForeColor = System.Drawing.Color.Red;
            this.lblProduct_Name.Name = "lblProduct_Name";
            this.lblProduct_Name.Size = new System.Drawing.Size(306, 50);
            this.lblProduct_Name.Text = "[Product_Title] + \" \" + [Product_Version]";
            // 
            // panelFormMain
            // 
            this.panelFormMain.BackColor = System.Drawing.Color.Transparent;
            this.panelFormMain.Controls.Add(this.tlpFormMain);
            this.panelFormMain.Controls.Add(this.tlpRunResetFunction);
            this.panelFormMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFormMain.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panelFormMain.Location = new System.Drawing.Point(0, 78);
            this.panelFormMain.Name = "panelFormMain";
            this.panelFormMain.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.panelFormMain.Size = new System.Drawing.Size(444, 494);
            this.panelFormMain.TabIndex = 57;
            // 
            // tlpFormMain
            // 
            this.tlpFormMain.ColumnCount = 1;
            this.tlpFormMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFormMain.Controls.Add(this.progressBarStatus_RunFunction, 0, 4);
            this.tlpFormMain.Controls.Add(this.grbCheckSumType, 0, 1);
            this.tlpFormMain.Controls.Add(this.grbInputFile, 0, 0);
            this.tlpFormMain.Controls.Add(this.grbComparisonCheckSum, 0, 3);
            this.tlpFormMain.Controls.Add(this.grbCheckSumInformation, 0, 2);
            this.tlpFormMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFormMain.Location = new System.Drawing.Point(20, 30);
            this.tlpFormMain.Name = "tlpFormMain";
            this.tlpFormMain.Padding = new System.Windows.Forms.Padding(5);
            this.tlpFormMain.RowCount = 5;
            this.tlpFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24F));
            this.tlpFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16F));
            this.tlpFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26F));
            this.tlpFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26F));
            this.tlpFormMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tlpFormMain.Size = new System.Drawing.Size(404, 398);
            this.tlpFormMain.TabIndex = 12;
            // 
            // progressBarStatus_RunFunction
            // 
            this.progressBarStatus_RunFunction.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarStatus_RunFunction.Location = new System.Drawing.Point(8, 368);
            this.progressBarStatus_RunFunction.Name = "progressBarStatus_RunFunction";
            this.progressBarStatus_RunFunction.Size = new System.Drawing.Size(388, 16);
            this.progressBarStatus_RunFunction.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarStatus_RunFunction.TabIndex = 8;
            // 
            // grbCheckSumType
            // 
            this.grbCheckSumType.BackColor = System.Drawing.Color.Transparent;
            this.grbCheckSumType.Controls.Add(this.cmbCheckSumType);
            this.grbCheckSumType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbCheckSumType.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbCheckSumType.Location = new System.Drawing.Point(8, 101);
            this.grbCheckSumType.Name = "grbCheckSumType";
            this.grbCheckSumType.Size = new System.Drawing.Size(388, 56);
            this.grbCheckSumType.TabIndex = 7;
            this.grbCheckSumType.TabStop = false;
            this.grbCheckSumType.Text = "CheckSum Type";
            // 
            // cmbCheckSumType
            // 
            this.cmbCheckSumType.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.cmbCheckSumType.FormattingEnabled = true;
            this.cmbCheckSumType.Items.AddRange(new object[] {
            "Select a type ...",
            "MD5",
            "SHA1‐160",
            "SHA2‐256",
            "SHA2‐384",
            "SHA2‐512"});
            this.cmbCheckSumType.Location = new System.Drawing.Point(3, 30);
            this.cmbCheckSumType.Name = "cmbCheckSumType";
            this.cmbCheckSumType.Size = new System.Drawing.Size(382, 23);
            this.cmbCheckSumType.TabIndex = 0;
            // 
            // grbInputFile
            // 
            this.grbInputFile.BackColor = System.Drawing.Color.Transparent;
            this.grbInputFile.Controls.Add(this.checkBoxReadOnly_ShowInputFilePath);
            this.grbInputFile.Controls.Add(this.btnLoadFile);
            this.grbInputFile.Controls.Add(this.txtShowInputFilePath);
            this.grbInputFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbInputFile.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbInputFile.Location = new System.Drawing.Point(8, 8);
            this.grbInputFile.Name = "grbInputFile";
            this.grbInputFile.Size = new System.Drawing.Size(388, 87);
            this.grbInputFile.TabIndex = 6;
            this.grbInputFile.TabStop = false;
            this.grbInputFile.Text = "Input File";
            // 
            // checkBoxReadOnly_ShowInputFilePath
            // 
            this.checkBoxReadOnly_ShowInputFilePath.AutoSize = true;
            this.checkBoxReadOnly_ShowInputFilePath.Checked = true;
            this.checkBoxReadOnly_ShowInputFilePath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxReadOnly_ShowInputFilePath.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.checkBoxReadOnly_ShowInputFilePath.Location = new System.Drawing.Point(304, 38);
            this.checkBoxReadOnly_ShowInputFilePath.Name = "checkBoxReadOnly_ShowInputFilePath";
            this.checkBoxReadOnly_ShowInputFilePath.Size = new System.Drawing.Size(78, 18);
            this.checkBoxReadOnly_ShowInputFilePath.TabIndex = 12;
            this.checkBoxReadOnly_ShowInputFilePath.Text = "isReadOnly";
            this.checkBoxReadOnly_ShowInputFilePath.UseVisualStyleBackColor = true;
            this.checkBoxReadOnly_ShowInputFilePath.CheckedChanged += new System.EventHandler(this.checkBoxReadOnly_ShowInputFilePath_CheckedChanged);
            // 
            // btnLoadFile
            // 
            this.btnLoadFile.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnLoadFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLoadFile.Location = new System.Drawing.Point(6, 31);
            this.btnLoadFile.Name = "btnLoadFile";
            this.btnLoadFile.Size = new System.Drawing.Size(90, 25);
            this.btnLoadFile.TabIndex = 5;
            this.btnLoadFile.Text = "Load File";
            this.btnLoadFile.UseVisualStyleBackColor = false;
            this.btnLoadFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // txtShowInputFilePath
            // 
            this.txtShowInputFilePath.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtShowInputFilePath.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtShowInputFilePath.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtShowInputFilePath.Location = new System.Drawing.Point(3, 62);
            this.txtShowInputFilePath.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtShowInputFilePath.Name = "txtShowInputFilePath";
            this.txtShowInputFilePath.ReadOnly = true;
            this.txtShowInputFilePath.Size = new System.Drawing.Size(382, 22);
            this.txtShowInputFilePath.TabIndex = 4;
            // 
            // grbComparisonCheckSum
            // 
            this.grbComparisonCheckSum.BackColor = System.Drawing.Color.Transparent;
            this.grbComparisonCheckSum.Controls.Add(this.lblResultComparison);
            this.grbComparisonCheckSum.Controls.Add(this.btnClear_ComparisonCheckSum);
            this.grbComparisonCheckSum.Controls.Add(this.btnCheckingComparison);
            this.grbComparisonCheckSum.Controls.Add(this.btnPaste_ComparisonCheckSum);
            this.grbComparisonCheckSum.Controls.Add(this.txtComparisonCheckSum);
            this.grbComparisonCheckSum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbComparisonCheckSum.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbComparisonCheckSum.Location = new System.Drawing.Point(8, 263);
            this.grbComparisonCheckSum.Name = "grbComparisonCheckSum";
            this.grbComparisonCheckSum.Size = new System.Drawing.Size(388, 94);
            this.grbComparisonCheckSum.TabIndex = 3;
            this.grbComparisonCheckSum.TabStop = false;
            this.grbComparisonCheckSum.Text = "Comparison CheckSum";
            // 
            // lblResultComparison
            // 
            this.lblResultComparison.AutoSize = true;
            this.lblResultComparison.ForeColor = System.Drawing.Color.Maroon;
            this.lblResultComparison.Location = new System.Drawing.Point(162, 41);
            this.lblResultComparison.Name = "lblResultComparison";
            this.lblResultComparison.Size = new System.Drawing.Size(53, 15);
            this.lblResultComparison.TabIndex = 10;
            this.lblResultComparison.Text = "Result ?";
            // 
            // btnClear_ComparisonCheckSum
            // 
            this.btnClear_ComparisonCheckSum.BackColor = System.Drawing.Color.LightGray;
            this.btnClear_ComparisonCheckSum.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnClear_ComparisonCheckSum.Location = new System.Drawing.Point(256, 34);
            this.btnClear_ComparisonCheckSum.Name = "btnClear_ComparisonCheckSum";
            this.btnClear_ComparisonCheckSum.Size = new System.Drawing.Size(60, 29);
            this.btnClear_ComparisonCheckSum.TabIndex = 9;
            this.btnClear_ComparisonCheckSum.Text = "Clear";
            this.btnClear_ComparisonCheckSum.UseVisualStyleBackColor = false;
            this.btnClear_ComparisonCheckSum.Click += new System.EventHandler(this.btnClear_ComparisonCheckSum_Click);
            // 
            // btnCheckingComparison
            // 
            this.btnCheckingComparison.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCheckingComparison.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckingComparison.Location = new System.Drawing.Point(6, 33);
            this.btnCheckingComparison.Name = "btnCheckingComparison";
            this.btnCheckingComparison.Size = new System.Drawing.Size(150, 30);
            this.btnCheckingComparison.TabIndex = 8;
            this.btnCheckingComparison.Text = "Checking Comparison";
            this.btnCheckingComparison.UseVisualStyleBackColor = false;
            this.btnCheckingComparison.Click += new System.EventHandler(this.btnCheckingComparison_Click);
            // 
            // btnPaste_ComparisonCheckSum
            // 
            this.btnPaste_ComparisonCheckSum.BackColor = System.Drawing.Color.LightGray;
            this.btnPaste_ComparisonCheckSum.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnPaste_ComparisonCheckSum.Location = new System.Drawing.Point(322, 34);
            this.btnPaste_ComparisonCheckSum.Name = "btnPaste_ComparisonCheckSum";
            this.btnPaste_ComparisonCheckSum.Size = new System.Drawing.Size(60, 29);
            this.btnPaste_ComparisonCheckSum.TabIndex = 7;
            this.btnPaste_ComparisonCheckSum.Text = "Paste";
            this.btnPaste_ComparisonCheckSum.UseVisualStyleBackColor = false;
            this.btnPaste_ComparisonCheckSum.Click += new System.EventHandler(this.btnPaste_ComparisonCheckSum_Click);
            // 
            // txtComparisonCheckSum
            // 
            this.txtComparisonCheckSum.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtComparisonCheckSum.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtComparisonCheckSum.Location = new System.Drawing.Point(3, 69);
            this.txtComparisonCheckSum.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtComparisonCheckSum.Name = "txtComparisonCheckSum";
            this.txtComparisonCheckSum.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtComparisonCheckSum.Size = new System.Drawing.Size(382, 22);
            this.txtComparisonCheckSum.TabIndex = 6;
            // 
            // grbCheckSumInformation
            // 
            this.grbCheckSumInformation.BackColor = System.Drawing.Color.Transparent;
            this.grbCheckSumInformation.Controls.Add(this.checkBoxReadOnly_CHSInfo);
            this.grbCheckSumInformation.Controls.Add(this.lblCheckSumType);
            this.grbCheckSumInformation.Controls.Add(this.btnToLower_CheckSum);
            this.grbCheckSumInformation.Controls.Add(this.btnToUpper_CheckSum);
            this.grbCheckSumInformation.Controls.Add(this.btnCopy_CheckSum);
            this.grbCheckSumInformation.Controls.Add(this.txtCheckSumInformation);
            this.grbCheckSumInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbCheckSumInformation.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbCheckSumInformation.Location = new System.Drawing.Point(8, 163);
            this.grbCheckSumInformation.Name = "grbCheckSumInformation";
            this.grbCheckSumInformation.Size = new System.Drawing.Size(388, 94);
            this.grbCheckSumInformation.TabIndex = 2;
            this.grbCheckSumInformation.TabStop = false;
            this.grbCheckSumInformation.Text = "CheckSum Information";
            // 
            // checkBoxReadOnly_CHSInfo
            // 
            this.checkBoxReadOnly_CHSInfo.AutoSize = true;
            this.checkBoxReadOnly_CHSInfo.Checked = true;
            this.checkBoxReadOnly_CHSInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxReadOnly_CHSInfo.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.checkBoxReadOnly_CHSInfo.Location = new System.Drawing.Point(9, 30);
            this.checkBoxReadOnly_CHSInfo.Name = "checkBoxReadOnly_CHSInfo";
            this.checkBoxReadOnly_CHSInfo.Size = new System.Drawing.Size(78, 18);
            this.checkBoxReadOnly_CHSInfo.TabIndex = 11;
            this.checkBoxReadOnly_CHSInfo.Text = "isReadOnly";
            this.checkBoxReadOnly_CHSInfo.UseVisualStyleBackColor = true;
            this.checkBoxReadOnly_CHSInfo.CheckedChanged += new System.EventHandler(this.checkBoxReadOnly_CheckedChanged);
            // 
            // lblCheckSumType
            // 
            this.lblCheckSumType.AutoSize = true;
            this.lblCheckSumType.Location = new System.Drawing.Point(6, 51);
            this.lblCheckSumType.Name = "lblCheckSumType";
            this.lblCheckSumType.Size = new System.Drawing.Size(100, 15);
            this.lblCheckSumType.TabIndex = 10;
            this.lblCheckSumType.Text = "CheckSum Type:";
            // 
            // btnToLower_CheckSum
            // 
            this.btnToLower_CheckSum.BackColor = System.Drawing.Color.Silver;
            this.btnToLower_CheckSum.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnToLower_CheckSum.Location = new System.Drawing.Point(190, 34);
            this.btnToLower_CheckSum.Name = "btnToLower_CheckSum";
            this.btnToLower_CheckSum.Size = new System.Drawing.Size(60, 29);
            this.btnToLower_CheckSum.TabIndex = 9;
            this.btnToLower_CheckSum.Text = "ToLower";
            this.btnToLower_CheckSum.UseVisualStyleBackColor = false;
            this.btnToLower_CheckSum.Click += new System.EventHandler(this.btnToLower_CheckSum_Click);
            // 
            // btnToUpper_CheckSum
            // 
            this.btnToUpper_CheckSum.BackColor = System.Drawing.Color.Silver;
            this.btnToUpper_CheckSum.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnToUpper_CheckSum.Location = new System.Drawing.Point(256, 34);
            this.btnToUpper_CheckSum.Name = "btnToUpper_CheckSum";
            this.btnToUpper_CheckSum.Size = new System.Drawing.Size(60, 29);
            this.btnToUpper_CheckSum.TabIndex = 8;
            this.btnToUpper_CheckSum.Text = "ToUpper";
            this.btnToUpper_CheckSum.UseVisualStyleBackColor = false;
            this.btnToUpper_CheckSum.Click += new System.EventHandler(this.btnToUpper_CheckSum_Click);
            // 
            // btnCopy_CheckSum
            // 
            this.btnCopy_CheckSum.BackColor = System.Drawing.Color.LightGray;
            this.btnCopy_CheckSum.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCopy_CheckSum.Location = new System.Drawing.Point(322, 34);
            this.btnCopy_CheckSum.Name = "btnCopy_CheckSum";
            this.btnCopy_CheckSum.Size = new System.Drawing.Size(60, 29);
            this.btnCopy_CheckSum.TabIndex = 6;
            this.btnCopy_CheckSum.Text = "Copy";
            this.btnCopy_CheckSum.UseVisualStyleBackColor = false;
            this.btnCopy_CheckSum.Click += new System.EventHandler(this.btnCopy_CheckSum_Click);
            // 
            // txtCheckSumInformation
            // 
            this.txtCheckSumInformation.BackColor = System.Drawing.SystemColors.Info;
            this.txtCheckSumInformation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtCheckSumInformation.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtCheckSumInformation.Location = new System.Drawing.Point(3, 69);
            this.txtCheckSumInformation.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtCheckSumInformation.Name = "txtCheckSumInformation";
            this.txtCheckSumInformation.ReadOnly = true;
            this.txtCheckSumInformation.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtCheckSumInformation.Size = new System.Drawing.Size(382, 22);
            this.txtCheckSumInformation.TabIndex = 5;
            // 
            // tlpRunResetFunction
            // 
            this.tlpRunResetFunction.ColumnCount = 3;
            this.tlpRunResetFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunResetFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunResetFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunResetFunction.Controls.Add(this.btnResetFunction, 2, 0);
            this.tlpRunResetFunction.Controls.Add(this.btnRunFunction, 0, 0);
            this.tlpRunResetFunction.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tlpRunResetFunction.Location = new System.Drawing.Point(20, 428);
            this.tlpRunResetFunction.Margin = new System.Windows.Forms.Padding(5);
            this.tlpRunResetFunction.Name = "tlpRunResetFunction";
            this.tlpRunResetFunction.RowCount = 1;
            this.tlpRunResetFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRunResetFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tlpRunResetFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tlpRunResetFunction.Size = new System.Drawing.Size(404, 46);
            this.tlpRunResetFunction.TabIndex = 11;
            // 
            // btnResetFunction
            // 
            this.btnResetFunction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.btnResetFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnResetFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetFunction.Location = new System.Drawing.Point(273, 5);
            this.btnResetFunction.Margin = new System.Windows.Forms.Padding(5);
            this.btnResetFunction.Name = "btnResetFunction";
            this.btnResetFunction.Padding = new System.Windows.Forms.Padding(3);
            this.btnResetFunction.Size = new System.Drawing.Size(126, 36);
            this.btnResetFunction.TabIndex = 3;
            this.btnResetFunction.Text = "Reset";
            this.btnResetFunction.UseVisualStyleBackColor = false;
            this.btnResetFunction.Click += new System.EventHandler(this.btnResetFunction_Click);
            // 
            // btnRunFunction
            // 
            this.btnRunFunction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(105)))), ((int)(((byte)(255)))));
            this.btnRunFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRunFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRunFunction.Location = new System.Drawing.Point(5, 5);
            this.btnRunFunction.Margin = new System.Windows.Forms.Padding(5);
            this.btnRunFunction.Name = "btnRunFunction";
            this.btnRunFunction.Padding = new System.Windows.Forms.Padding(3);
            this.btnRunFunction.Size = new System.Drawing.Size(124, 36);
            this.btnRunFunction.TabIndex = 2;
            this.btnRunFunction.Text = "Run";
            this.btnRunFunction.UseVisualStyleBackColor = false;
            this.btnRunFunction.Click += new System.EventHandler(this.btnRunFunction_Click);
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStripStatus.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressBarStatus_Function,
            this.lblTimerStatus_Lasted,
            this.lblStatus_Procces});
            this.toolStripStatus.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStripStatus.Location = new System.Drawing.Point(0, 573);
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Padding = new System.Windows.Forms.Padding(5);
            this.toolStripStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripStatus.Size = new System.Drawing.Size(444, 28);
            this.toolStripStatus.TabIndex = 58;
            this.toolStripStatus.Text = "toolStrip Status";
            // 
            // progressBarStatus_Function
            // 
            this.progressBarStatus_Function.Name = "progressBarStatus_Function";
            this.progressBarStatus_Function.Size = new System.Drawing.Size(170, 16);
            this.progressBarStatus_Function.Step = 2;
            this.progressBarStatus_Function.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBarStatus_Function.Value = 50;
            this.progressBarStatus_Function.Visible = false;
            // 
            // lblTimerStatus_Lasted
            // 
            this.lblTimerStatus_Lasted.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblTimerStatus_Lasted.ForeColor = System.Drawing.Color.DarkBlue;
            this.lblTimerStatus_Lasted.Name = "lblTimerStatus_Lasted";
            this.lblTimerStatus_Lasted.Size = new System.Drawing.Size(42, 15);
            this.lblTimerStatus_Lasted.Text = "Time ?";
            // 
            // timerStatus_Function
            // 
            this.timerStatus_Function.Interval = 1000;
            this.timerStatus_Function.Tick += new System.EventHandler(this.timerStatus_Function_Tick);
            // 
            // lblProductLogo
            // 
            this.lblProductLogo.AutoSize = false;
            this.lblProductLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.Size = new System.Drawing.Size(50, 50);
            this.lblProductLogo.Text = "Product.Logo";
            // 
            // lblStatus_Procces
            // 
            this.lblStatus_Procces.ForeColor = System.Drawing.SystemColors.InfoText;
            this.lblStatus_Procces.Name = "lblStatus_Procces";
            this.lblStatus_Procces.Size = new System.Drawing.Size(91, 15);
            this.lblStatus_Procces.Text = "Status Procces ?";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(444, 601);
            this.ControlBox = false;
            this.Controls.Add(this.toolStripStatus);
            this.Controls.Add(this.panelFormMain);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CheckSum Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            this.panelFormMain.ResumeLayout(false);
            this.tlpFormMain.ResumeLayout(false);
            this.grbCheckSumType.ResumeLayout(false);
            this.grbInputFile.ResumeLayout(false);
            this.grbInputFile.PerformLayout();
            this.grbComparisonCheckSum.ResumeLayout(false);
            this.grbComparisonCheckSum.PerformLayout();
            this.grbCheckSumInformation.ResumeLayout(false);
            this.grbCheckSumInformation.PerformLayout();
            this.tlpRunResetFunction.ResumeLayout(false);
            this.toolStripStatus.ResumeLayout(false);
            this.toolStripStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.ToolStripButton btnMinimized;
        private System.Windows.Forms.ToolStripButton btnAbout;
        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblProduct_Name;
        private System.Windows.Forms.Panel panelFormMain;
        private System.Windows.Forms.ToolStrip toolStripStatus;
        private System.Windows.Forms.ToolStripProgressBar progressBarStatus_Function;
        private System.Windows.Forms.ToolStripLabel lblTimerStatus_Lasted;
        private System.Windows.Forms.Timer timerStatus_Function;
        private System.Windows.Forms.TableLayoutPanel tlpRunResetFunction;
        private System.Windows.Forms.Button btnResetFunction;
        private System.Windows.Forms.Button btnRunFunction;
        private System.Windows.Forms.TableLayoutPanel tlpFormMain;
        private System.Windows.Forms.GroupBox grbCheckSumType;
        private System.Windows.Forms.ComboBox cmbCheckSumType;
        private System.Windows.Forms.GroupBox grbInputFile;
        private System.Windows.Forms.CheckBox checkBoxReadOnly_ShowInputFilePath;
        private System.Windows.Forms.Button btnLoadFile;
        private System.Windows.Forms.TextBox txtShowInputFilePath;
        private System.Windows.Forms.GroupBox grbComparisonCheckSum;
        private System.Windows.Forms.Label lblResultComparison;
        private System.Windows.Forms.Button btnClear_ComparisonCheckSum;
        private System.Windows.Forms.Button btnCheckingComparison;
        private System.Windows.Forms.Button btnPaste_ComparisonCheckSum;
        private System.Windows.Forms.TextBox txtComparisonCheckSum;
        private System.Windows.Forms.GroupBox grbCheckSumInformation;
        private System.Windows.Forms.CheckBox checkBoxReadOnly_CHSInfo;
        private System.Windows.Forms.Label lblCheckSumType;
        private System.Windows.Forms.Button btnToLower_CheckSum;
        private System.Windows.Forms.Button btnToUpper_CheckSum;
        private System.Windows.Forms.Button btnCopy_CheckSum;
        private System.Windows.Forms.TextBox txtCheckSumInformation;
        private System.Windows.Forms.ProgressBar progressBarStatus_RunFunction;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblStatus_Procces;
    }
}