﻿namespace HiVE.CHSInfo.isForm
{
    partial class frmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAbout));
            this.panelCopyright = new System.Windows.Forms.Panel();
            this.lblProduct_Copyright = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.picCompanyLogo = new System.Windows.Forms.PictureBox();
            this.picProductLogo = new System.Windows.Forms.PictureBox();
            this.flpDetails = new System.Windows.Forms.FlowLayoutPanel();
            this.lblProduct_Title = new System.Windows.Forms.Label();
            this.lblProduct_Version = new System.Windows.Forms.Label();
            this.lblProduct_Description = new System.Windows.Forms.Label();
            this.lblProduct_Type = new System.Windows.Forms.Label();
            this.lblProduct_MoreDescription = new System.Windows.Forms.Label();
            this.linkLblCompany_WebSite = new System.Windows.Forms.LinkLabel();
            this.linkLblCompany_Email = new System.Windows.Forms.LinkLabel();
            this.panelCopyright.SuspendLayout();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCompanyLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductLogo)).BeginInit();
            this.flpDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelCopyright
            // 
            this.panelCopyright.Controls.Add(this.lblProduct_Copyright);
            this.panelCopyright.Controls.Add(this.btnClose);
            this.panelCopyright.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelCopyright.Location = new System.Drawing.Point(0, 391);
            this.panelCopyright.Name = "panelCopyright";
            this.panelCopyright.Padding = new System.Windows.Forms.Padding(5);
            this.panelCopyright.Size = new System.Drawing.Size(624, 50);
            this.panelCopyright.TabIndex = 0;
            // 
            // lblProduct_Copyright
            // 
            this.lblProduct_Copyright.AutoSize = true;
            this.lblProduct_Copyright.Location = new System.Drawing.Point(12, 20);
            this.lblProduct_Copyright.Name = "lblProduct_Copyright";
            this.lblProduct_Copyright.Size = new System.Drawing.Size(100, 13);
            this.lblProduct_Copyright.TabIndex = 1;
            this.lblProduct_Copyright.Text = "[Product_Copyright]";
            this.lblProduct_Copyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(537, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "OK";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.Controls.Add(this.picCompanyLogo);
            this.panelLogo.Controls.Add(this.picProductLogo);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Padding = new System.Windows.Forms.Padding(10);
            this.panelLogo.Size = new System.Drawing.Size(200, 391);
            this.panelLogo.TabIndex = 1;
            // 
            // picCompanyLogo
            // 
            this.picCompanyLogo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.picCompanyLogo.Location = new System.Drawing.Point(10, 201);
            this.picCompanyLogo.Name = "picCompanyLogo";
            this.picCompanyLogo.Size = new System.Drawing.Size(180, 180);
            this.picCompanyLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCompanyLogo.TabIndex = 1;
            this.picCompanyLogo.TabStop = false;
            // 
            // picProductLogo
            // 
            this.picProductLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.picProductLogo.Location = new System.Drawing.Point(10, 10);
            this.picProductLogo.Name = "picProductLogo";
            this.picProductLogo.Size = new System.Drawing.Size(180, 180);
            this.picProductLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picProductLogo.TabIndex = 0;
            this.picProductLogo.TabStop = false;
            // 
            // flpDetails
            // 
            this.flpDetails.Controls.Add(this.lblProduct_Title);
            this.flpDetails.Controls.Add(this.lblProduct_Version);
            this.flpDetails.Controls.Add(this.lblProduct_Description);
            this.flpDetails.Controls.Add(this.lblProduct_Type);
            this.flpDetails.Controls.Add(this.lblProduct_MoreDescription);
            this.flpDetails.Controls.Add(this.linkLblCompany_WebSite);
            this.flpDetails.Controls.Add(this.linkLblCompany_Email);
            this.flpDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDetails.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpDetails.Location = new System.Drawing.Point(200, 0);
            this.flpDetails.Name = "flpDetails";
            this.flpDetails.Padding = new System.Windows.Forms.Padding(15);
            this.flpDetails.Size = new System.Drawing.Size(424, 391);
            this.flpDetails.TabIndex = 6;
            // 
            // lblProduct_Title
            // 
            this.lblProduct_Title.AutoSize = true;
            this.lblProduct_Title.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold);
            this.lblProduct_Title.ForeColor = System.Drawing.Color.Red;
            this.lblProduct_Title.Location = new System.Drawing.Point(18, 18);
            this.lblProduct_Title.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.lblProduct_Title.Name = "lblProduct_Title";
            this.lblProduct_Title.Size = new System.Drawing.Size(121, 19);
            this.lblProduct_Title.TabIndex = 313;
            this.lblProduct_Title.Text = "[Product_Title]";
            this.lblProduct_Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProduct_Version
            // 
            this.lblProduct_Version.AutoSize = true;
            this.lblProduct_Version.Font = new System.Drawing.Font("Cambria", 10F);
            this.lblProduct_Version.Location = new System.Drawing.Point(18, 55);
            this.lblProduct_Version.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.lblProduct_Version.Name = "lblProduct_Version";
            this.lblProduct_Version.Size = new System.Drawing.Size(159, 16);
            this.lblProduct_Version.TabIndex = 337;
            this.lblProduct_Version.Text = "Version [ProductVersion]";
            this.lblProduct_Version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProduct_Description
            // 
            this.lblProduct_Description.AutoSize = true;
            this.lblProduct_Description.Font = new System.Drawing.Font("Cambria", 10F);
            this.lblProduct_Description.Location = new System.Drawing.Point(18, 84);
            this.lblProduct_Description.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.lblProduct_Description.Name = "lblProduct_Description";
            this.lblProduct_Description.Size = new System.Drawing.Size(141, 16);
            this.lblProduct_Description.TabIndex = 339;
            this.lblProduct_Description.Text = "[Product_Description]";
            this.lblProduct_Description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProduct_Type
            // 
            this.lblProduct_Type.AutoSize = true;
            this.lblProduct_Type.Font = new System.Drawing.Font("Cambria", 10F);
            this.lblProduct_Type.Location = new System.Drawing.Point(18, 113);
            this.lblProduct_Type.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.lblProduct_Type.Name = "lblProduct_Type";
            this.lblProduct_Type.Size = new System.Drawing.Size(103, 16);
            this.lblProduct_Type.TabIndex = 340;
            this.lblProduct_Type.Text = "[Product_TYPE]";
            this.lblProduct_Type.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProduct_MoreDescription
            // 
            this.lblProduct_MoreDescription.AutoSize = true;
            this.lblProduct_MoreDescription.Font = new System.Drawing.Font("Cambria", 10F);
            this.lblProduct_MoreDescription.Location = new System.Drawing.Point(18, 142);
            this.lblProduct_MoreDescription.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.lblProduct_MoreDescription.Name = "lblProduct_MoreDescription";
            this.lblProduct_MoreDescription.Size = new System.Drawing.Size(172, 16);
            this.lblProduct_MoreDescription.TabIndex = 342;
            this.lblProduct_MoreDescription.Text = "[Product_MoreDescription]";
            this.lblProduct_MoreDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // linkLblCompany_WebSite
            // 
            this.linkLblCompany_WebSite.AutoSize = true;
            this.linkLblCompany_WebSite.Font = new System.Drawing.Font("Cambria", 11F);
            this.linkLblCompany_WebSite.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLblCompany_WebSite.Location = new System.Drawing.Point(18, 171);
            this.linkLblCompany_WebSite.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.linkLblCompany_WebSite.Name = "linkLblCompany_WebSite";
            this.linkLblCompany_WebSite.Size = new System.Drawing.Size(134, 17);
            this.linkLblCompany_WebSite.TabIndex = 338;
            this.linkLblCompany_WebSite.TabStop = true;
            this.linkLblCompany_WebSite.Text = "[Company_WebSite]";
            this.linkLblCompany_WebSite.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // linkLblCompany_Email
            // 
            this.linkLblCompany_Email.AutoSize = true;
            this.linkLblCompany_Email.Font = new System.Drawing.Font("Cambria", 11F);
            this.linkLblCompany_Email.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLblCompany_Email.Location = new System.Drawing.Point(18, 201);
            this.linkLblCompany_Email.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.linkLblCompany_Email.Name = "linkLblCompany_Email";
            this.linkLblCompany_Email.Size = new System.Drawing.Size(119, 17);
            this.linkLblCompany_Email.TabIndex = 341;
            this.linkLblCompany_Email.TabStop = true;
            this.linkLblCompany_Email.Text = "[Company_Email]";
            this.linkLblCompany_Email.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.flpDetails);
            this.Controls.Add(this.panelLogo);
            this.Controls.Add(this.panelCopyright);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAbout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About CheckSum Information";
            this.Load += new System.EventHandler(this.frmAbout_Load);
            this.panelCopyright.ResumeLayout(false);
            this.panelCopyright.PerformLayout();
            this.panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picCompanyLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductLogo)).EndInit();
            this.flpDetails.ResumeLayout(false);
            this.flpDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelCopyright;
        private System.Windows.Forms.Label lblProduct_Copyright;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.FlowLayoutPanel flpDetails;
        private System.Windows.Forms.PictureBox picCompanyLogo;
        private System.Windows.Forms.PictureBox picProductLogo;
        private System.Windows.Forms.Label lblProduct_Title;
        private System.Windows.Forms.Label lblProduct_Version;
        private System.Windows.Forms.LinkLabel linkLblCompany_WebSite;
        private System.Windows.Forms.Label lblProduct_Description;
        private System.Windows.Forms.Label lblProduct_Type;
        private System.Windows.Forms.Label lblProduct_MoreDescription;
        private System.Windows.Forms.LinkLabel linkLblCompany_Email;
    }
}