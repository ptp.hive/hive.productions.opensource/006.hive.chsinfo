﻿using HiVE.CHSInfo.isClass;
using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace HiVE.CHSInfo.isForm
{
    public partial class frmMain : Form
    {
        #region ConnectionTryCatch
        //----------

        private TryCatch _TryCatch = new TryCatch();
        private static string _GetMEC_Forms = string.Empty;
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionTryCatch

        public frmMain()
        {
            InitializeComponent();

            try
            {
                _GetMEC_Forms = _TryCatch.GetMEC_FormMain;

                btnAbout.Text = "About " + "CheckSum Information";
                lblProductLogo.BackgroundImage = HiVE.isResources.res_HiVE.logo_x256;
                lblProduct_Name.Text =
                    "CheckSum Information" +
                    " " +
                    Application.ProductVersion.Substring(0, 2);
            }
            catch (Exception exc)
            {
                this.DialogResult = DialogResult.No;
                this.Close();
            }
        }

        #region ConnectionModels
        //----------

        //----------
        #endregion ConnectionModels

        #region MultiThreaded
        //----------

        private DateTime dtStartProcess = DateTime.Now;
        private TimeSpan tsEndToStart;
        //
        private Thread trLoadFile;
        private ThreadStart thrsLoadFile;
        //
        private Thread trRunFunction;
        private ThreadStart thrsRunFunction;
        //
        private Thread trTimer;
        private ThreadStart thrsTimer;

        //----------
        #endregion MultiThreaded

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Form Main
        //----------

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch { }
        }

        private void btnMinimized_Click(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Minimized;
            }
            catch { }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(
                    "Exit from the application?",
                    "Exit",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            try
            {
                frmAbout _frmAbout = new frmAbout();
                HiVE.WinSDS.WinShowShutForm ssf = new HiVE.WinSDS.WinShowShutForm(new frmAbout());
                _frmAbout.Close();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0100: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در بارگیری درباره ما
                    MessageError = "Error loading About Us. ";

                    MesExpErrorForms += _GetMEC_Forms + "0100: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //----------
        #endregion Form Main

        #region Timer Status
        //----------

        private void TimerStatus_Start()
        {
            try
            {
                progressBarStatus_RunFunction.Value = 0;
                progressBarStatus_Function.Value = 0;
                //lblStatus_Procces.Text = "0 % Procces";
                lblStatus_Procces.Text = "Process Started ...!";
                //
                dtStartProcess = DateTime.Now;
                tsEndToStart = dtStartProcess.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatus_Lasted.Text = tsEndToStart.ToString(); }));

                timerStatus_Function.Enabled = true;
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_FormMain + "0200: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در شروع تایمر
                    MessageError = "Error starting the timer. ";

                    MesExpErrorForms += _TryCatch.GetMEC_FormMain + "0200: " + MessageError;
                }
            }
        }

        private void TimerStatus_Counter()
        {
            try
            {
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatus_Lasted.Text = tsEndToStart.ToString(); }));
                //
                //Invoke(new MethodInvoker(delegate () { progressBarStatus_Function.Value += 1; }));
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0300: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در بروزرسانی شمارنده تایمر
                    MessageError = "Error for update the counter of timer. ";

                    MesExpErrorForms += _GetMEC_Forms + "0300: " + MessageError;
                }
            }
        }

        private void timerStatus_Function_Tick(object sender, EventArgs e)
        {
            try
            {
                thrsTimer = new ThreadStart(TimerStatus_Counter);
                trTimer = new Thread(thrsTimer);
                trTimer.Priority = ThreadPriority.AboveNormal;
                //
                trTimer.Start();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0400: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در مارنده تایمر
                    MessageError = "Error in counter of timer. ";

                    MesExpErrorForms += _GetMEC_Forms + "0400: " + MessageError;
                }
            }
        }

        private void TimerStatus_Stop()
        {
            try
            {
                timerStatus_Function.Enabled = false;
                //
                tsEndToStart = DateTime.Now.Subtract(dtStartProcess);
                Invoke(new MethodInvoker(delegate () { lblTimerStatus_Lasted.Text = tsEndToStart.ToString(); }));
                Invoke(new MethodInvoker(delegate () { lblStatus_Procces.Text = "100 % Procces"; }));
                //
                Invoke(new MethodInvoker(delegate () { progressBarStatus_Function.Value = 100; }));
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0500: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در پایان تایمر
                    MessageError = "Error stoping the timer. ";

                    MesExpErrorForms += _GetMEC_Forms + "0500: " + MessageError;
                }
            }
        }

        //----------
        #endregion Timer Status

        #region Function Type
        //----------

        private static string GetDefaultInputPath =
            System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        private string GetLastInputPath = string.Empty;
        private string MessageRunFunction = string.Empty;
        private string MessageRunComparison = string.Empty;
        private bool isRunFunction = false;
        private int AsynsCompletedEventStatus = -2;

        private bool ResetFormMain()
        {
            try
            {
                checkBoxReadOnly_ShowInputFilePath.Checked = true;
                txtShowInputFilePath.Text = string.Empty;
                txtShowInputFilePath.ReadOnly = checkBoxReadOnly_ShowInputFilePath.Checked;
                //
                cmbCheckSumType.SelectedIndex = 0;
                //
                checkBoxReadOnly_CHSInfo.Checked = true;
                txtCheckSumInformation.Text = string.Empty;
                txtCheckSumInformation.ReadOnly = checkBoxReadOnly_CHSInfo.Checked;
                //
                txtComparisonCheckSum.Text = string.Empty;
                //
                RefreshRunCancleFunction(false);
                //
                lblResultComparison.Text = "Result ?";
                //
                lblStatus_Procces.Text = string.Empty;
                lblTimerStatus_Lasted.Text = string.Empty;
                timerStatus_Function.Enabled = false;
                progressBarStatus_RunFunction.Value = 0;
                progressBarStatus_Function.Value = 0;
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0600: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تنظیم مجدد فرم اصلی
                    MessageError = "Error for refresh main form. ";

                    MesExpErrorForms += _GetMEC_Forms + "0600: " + MessageError;
                }

                return false;
            }
            return true;
        }

        private void RefreshRunCancleFunction(bool isRun)
        {
            try
            {
                tlpFormMain.Enabled = !isRun;
                progressBarStatus_RunFunction.Visible = isRun;

                btnRunFunction.Enabled = !isRun;
                //btnResetFunction.Enabled = !isRun;

                if (isRun)
                {
                    btnResetFunction.Text = "Cancle";
                }
                else
                {
                    btnResetFunction.Text = "Reset";
                }
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0700: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تازه کردن اجرا/عدم اجرای فرآیند
                    MessageError = "Error for refresh Run/Cancle function. ";

                    MesExpErrorForms += _GetMEC_Forms + "0700: " + MessageError;
                }
            }
        }

        //----------
        #endregion Function Type

        #region Load Input File
        //----------

        private void LoadFile()
        {
            try
            {
                OpenFileDialog opfLoadFile = new OpenFileDialog();

                Invoke(new MethodInvoker(delegate ()
                {
                    opfLoadFile.InitialDirectory = GetDefaultInputPath;
                    try
                    {
                        if (txtShowInputFilePath.Text != null && txtShowInputFilePath.Text != "")
                        {
                            /// Checking Exists Last Input Path and File
                            if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(txtShowInputFilePath.Text)))
                            {
                                opfLoadFile.InitialDirectory =
                                    System.IO.Directory.CreateDirectory(
                                        System.IO.Path.GetDirectoryName(txtShowInputFilePath.Text))
                                        .ToString();
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        if (!_TryCatch.GetShowFriendlyMessage)
                        {
                            MesExpErrorForms += _GetMEC_Forms + "0810: " + exc.Message;
                        }
                        else
                        {
                            string MessageError = string.Empty;

                            /// خطا در بارگیری مسیر فایل ورودی
                            MessageError = "Error loading input file path. ";

                            MesExpErrorForms += _GetMEC_Forms + "0810: " + MessageError;
                        }
                    }
                }));

                opfLoadFile.Title = "Please select an file to Inputed.";
                opfLoadFile.Multiselect = false;

                opfLoadFile.Filter =
                                    "All Files (*.*)|*.*" +
                              "|" + "ISO Files (*.iso)|*.iso" +
                              "|" + "EXE Files (*.exe; *.msi)|*.exe; *.msi" +
                              "|" + "ZIP Files (*.zip; *.rar; *.7z; *.tar)|*.zip; *.rar; *.7z; *.tar" +
                              "|" + "Media Files (*.3gp; *.3g2; *.avi)|*.3gp; *.3g2; *.avi" +
                              "|" + "Video Files (*.3gp; *.avi; *.flv; *.mp4; *.mpg; *.mpeg; *.mov; *.wmv)|*.3gp; *.avi; *.flv; *.mp4; *.mpg; *.mpeg; *.mov; *.wmv" +
                              "|" + "Audio Files (*.aac; *.ac3; *.mp3; *.wav; *.ra|*.aac; *.ac3; *.mp3; *.wav; *.ra" +
                              "|" + "Database Files (*.db; *.db3; *.sqlite)|*.db; *.db3; *.sqlite";

                Invoke(new MethodInvoker(delegate ()
                {
                    if (opfLoadFile.ShowDialog() == DialogResult.OK)
                    {
                        txtShowInputFilePath.Text = opfLoadFile.FileName;
                    }
                    txtCheckSumInformation.Text = string.Empty;
                }));
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0800: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در بارگیری فایل ورودی
                    MessageError = "Error loading input file. ";

                    MesExpErrorForms += _GetMEC_Forms + "0800: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            try
            {
                thrsLoadFile = new ThreadStart(LoadFile);
                trLoadFile = new Thread(thrsLoadFile);
                trLoadFile.Priority = ThreadPriority.Normal;
                //
                trLoadFile.Start();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0900: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در اجرای عملیات بارگیری فایل ورودی
                    MessageError = "Error run function for loading input file. ";

                    MesExpErrorForms += _GetMEC_Forms + "0900: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void checkBoxReadOnly_ShowInputFilePath_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                txtShowInputFilePath.ReadOnly = checkBoxReadOnly_ShowInputFilePath.Checked;
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تغییر وضعیت فقط خواندنی
                    MessageError = "Error in change status ReadOnly. ";

                    MesExpErrorForms += _GetMEC_Forms + "1000: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //----------
        #endregion Load Input File

        #region CheckSum Type
        //----------

        //----------
        #endregion CheckSum Type

        #region CheckSum Information
        //----------

        private void checkBoxReadOnly_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                txtCheckSumInformation.ReadOnly = checkBoxReadOnly_CHSInfo.Checked;
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1100: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تغییر وضعیت فقط خواندنی
                    MessageError = "Error in change status ReadOnly. ";

                    MesExpErrorForms += _GetMEC_Forms + "1100: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnToLower_CheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                txtCheckSumInformation.Text = txtCheckSumInformation.Text.ToLower();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1200: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تنظیم حروف کوچک کردن
                    MessageError = "Error in set ToLower. ";

                    MesExpErrorForms += _GetMEC_Forms + "1200: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnToUpper_CheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                txtCheckSumInformation.Text = txtCheckSumInformation.Text.ToUpper();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1300: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در تنظیم حروف کوچک کردن
                    MessageError = "Error in set ToUpper. ";

                    MesExpErrorForms += _GetMEC_Forms + "1300: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnCopy_CheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(txtCheckSumInformation.Text);

                MessageBox.Show(
                    "Set in the Clipboard.",
                    "Clipboard",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information,
                    MessageBoxDefaultButton.Button1);
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1400: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا برای ذخیره در حافظه
                    MessageError = "Error for save in the clipboard. ";

                    MesExpErrorForms += _GetMEC_Forms + "1400: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //----------
        #endregion CheckSum Information

        #region Comparison CheckSum
        //----------

        private bool isErrorRunComparison()
        {
            bool isError = false;
            /// قسمت‌های زیر را تکمیل کنید
            MessageRunFunction = "Complete the fields below: \n";

            try
            {
                if (txtCheckSumInformation.Text == null || txtCheckSumInformation.Text == "")
                {
                    /// فایل ورودی را انتخاب نمایید
                    MessageRunFunction += "\n Choose the input file. ";
                    isError = true;
                }

                if (txtComparisonCheckSum.Text == null || txtComparisonCheckSum.Text == "")
                {
                    /// رقم کنترلی دوم را وارد نمایید
                    MessageRunComparison += "\n Enter the second checksum. ";
                    isError = true;
                }
            }
            catch (Exception exc)
            {
                isError = true;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1500: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// تعیین خطاهای اجرای عملیات مقایسه، با مشکل همراه بود.
                    MessageError = "Set errors compared operations, the problem was. ";

                    MesExpErrorForms += _GetMEC_Forms + "1500: " + MessageError;
                }
            }

            return isError;
        }

        private void btnCheckingComparison_Click(object sender, EventArgs e)
        {
            try
            {
                lblResultComparison.Text = "Result ?";

                if (!isErrorRunComparison())
                {
                    if (txtCheckSumInformation.Text.ToUpper() == txtComparisonCheckSum.Text.ToUpper())
                    {
                        lblResultComparison.Text = "AreEqual";
                    }
                    else
                    {
                        lblResultComparison.Text = "AreNotEqual";
                    }
                }
                else
                {
                    MessageBox.Show(
                        MessageRunComparison,
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1600: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در بررسی مقایسه
                    MessageError = "Error in checking comparison. ";

                    MesExpErrorForms += _GetMEC_Forms + "1600: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnClear_ComparisonCheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                txtComparisonCheckSum.Text = string.Empty;
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1700: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در پاک کردن رقم کنترلی دوم
                    MessageError = "Error in clear comparison checksum. ";

                    MesExpErrorForms += _GetMEC_Forms + "1700: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnPaste_ComparisonCheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                txtComparisonCheckSum.Text = Clipboard.GetText();
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1800: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا برای چسباندن از حافظه
                    MessageError = "Error for paste from the clipboard. ";

                    MesExpErrorForms += _GetMEC_Forms + "1800: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //----------
        #endregion Comparison CheckSum

        #region RunCancleFunction
        //----------

        private void RunFunction()
        {
            try
            {
                AsynsCompletedEventStatus = -2;

                isRunFunction = true;
                Invoke(new MethodInvoker(delegate () { RefreshRunCancleFunction(isRunFunction); }));

                Invoke(new MethodInvoker(delegate () { TimerStatus_Start(); }));

                Invoke(new MethodInvoker(delegate ()
                {
                    lblCheckSumType.Text = cmbCheckSumType.SelectedItem.ToString();
                    txtCheckSumInformation.Text = string.Empty;
                }));

                int CheckSumType = -1;
                string strFilePath = string.Empty;
                string strCheckSumInformation = string.Empty;

                #region Calculate Algoritm Hash Code Checksum
                //----------

                Invoke(new MethodInvoker(delegate ()
                {
                    CheckSumType = cmbCheckSumType.SelectedIndex;
                    strFilePath = txtShowInputFilePath.Text;
                }));

                int _selectedIndex = -1;

                /// Select a type ...
                if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation = string.Empty;
                }
                ///// CRC-32
                //else if (CheckSumType == ++_selectedIndex)
                //{
                //    strCheckSumInformation =
                //        HashGenerator.GetCRC32Checksum(strFilePath);
                //}
                ///// CRC-64
                //else if (CheckSumType == ++_selectedIndex)
                //{
                //    strCheckSumInformation =
                //        HashGenerator.GetCRC64Checksum(strFilePath);
                //}
                /// MD5
                else if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation =
                        HashGenerator.GetMD5Checksum(strFilePath);
                }
                /// SHA1‐160
                else if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation =
                        HashGenerator.GetSHA1Checksum(strFilePath);
                }
                /// SHA2‐256
                else if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation =
                        HashGenerator.GetSHA256Checksum(strFilePath);
                }
                /// SHA2‐384
                else if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation =
                        HashGenerator.GetSHA384Checksum(strFilePath);
                }
                /// SHA2‐512
                else if (CheckSumType == ++_selectedIndex)
                {
                    strCheckSumInformation =
                        HashGenerator.GetSHA512Checksum(strFilePath);
                }

                //----------
                #endregion Calculate Algoritm Hash Code Checksum

                Invoke(new MethodInvoker(delegate ()
                {
                    txtCheckSumInformation.Text = strCheckSumInformation;
                }));

                AsynsCompletedEventStatus = 1;
            }
            catch (Exception exc)
            {
                AsynsCompletedEventStatus = -1;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "1900: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در اجرای فرآیند
                    MessageError = "Error in the runing function. ";

                    MesExpErrorForms += _GetMEC_Forms + "1900: " + MessageError;
                }
            }

            Status_Function_RunWorkerCompleted();
        }

        private void Status_Function_RunWorkerCompleted()
        {
            try
            {
                TimerStatus_Stop();

                if (AsynsCompletedEventStatus == 0)
                {
                    /// عملیات متوقف شد
                    lblStatus_Procces.Text = "The operation stopped!";
                }
                else if (AsynsCompletedEventStatus == -1)
                {
                    /// عملیات با خطا همراه بود
                    lblStatus_Procces.Text = "An error has occurred!";
                }
                else if (AsynsCompletedEventStatus == 1)
                {
                    /// عملیات با موفقیت به پایان رسید
                    lblStatus_Procces.Text = "The operation ended successfully!";
                    //lblStatus_Procces.Text = "100 %";
                    progressBarStatus_Function.Value = 100;
                }

                isRunFunction = false;
                Invoke(new MethodInvoker(delegate ()
                {
                    RefreshRunCancleFunction(isRunFunction);
                }));
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "2000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در پایان اجرای فرآیند
                    MessageError = "Error in the end of run function. ";

                    MesExpErrorForms += _GetMEC_Forms + "2000: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private bool isErrorRunFunction()
        {
            bool isError = false;
            /// قسمت‌های زیر را تکمیل کنید
            MessageRunFunction = "Complete the fields below: \n";

            try
            {
                if (txtShowInputFilePath.Text == null || txtShowInputFilePath.Text == "")
                {
                    /// فایل ورودی را انتخاب نمایید
                    MessageRunFunction += "\n Choose the input file. ";
                    isError = true;
                }
                else
                {
                    /// Exists Input File Path
                    if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(txtShowInputFilePath.Text)))
                    {
                        /// مسیر فایل ورودی نامعتبر می‌باشد
                        MessageRunFunction += "\n The input file path is invalid. ";
                        isError = true;
                    }

                    /// Exists Input File
                    if (!System.IO.File.Exists(txtShowInputFilePath.Text))
                    {
                        /// مسیر فایل ورودی نامعتبر می‌باشد
                        MessageRunFunction += "\n The input file is invalid. ";
                        isError = true;
                    }
                }

                if (cmbCheckSumType.SelectedIndex == -1 || cmbCheckSumType.SelectedIndex == 0)
                {
                    /// نوع فرآیند مورد نظر را انتخاب نمایید
                    MessageRunFunction += "\n To choose the type of process. ";
                    isError = true;
                }
            }
            catch (Exception exc)
            {
                isError = true;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "2100: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// تعیین خطاهای اجرای عملیات مقایسه، با مشکل همراه بود.
                    MessageError = "Set errors compared operations, the problem was. ";

                    MesExpErrorForms += _GetMEC_Forms + "2100: " + MessageError;
                }
                isError = true;
            }

            return isError;
        }

        private void btnRunFunction_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isErrorRunFunction())
                {
                    thrsRunFunction = new ThreadStart(RunFunction);
                    trRunFunction = new Thread(thrsRunFunction);
                    trRunFunction.Priority = ThreadPriority.Normal;
                    trRunFunction.Name = "trRunFunction";
                    //
                    trRunFunction.Start();
                }
                else
                {
                    MessageBox.Show(
                        MessageRunFunction,
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "2200: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// خطا در اجرای عملیات
                    MessageError = "Error in run function. ";

                    MesExpErrorForms += _GetMEC_Forms + "2200: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnResetFunction_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isRunFunction)
                {
                    ResetFormMain();
                }
                else
                {
                    trRunFunction.Abort();

                    Process[] proc = Process.GetProcessesByName(trRunFunction.Name);

                    if (proc.Length != 0)
                    {
                        proc[0].Kill();

                        AsynsCompletedEventStatus = 0;
                        Status_Function_RunWorkerCompleted();
                    }
                }
            }
            catch (Exception exc)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "2300: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    if (!isRunFunction)
                    {
                        /// خطا در عملیات تنظیم مجدد
                        MessageError = "Error in the reset function. ";
                    }
                    else
                    {
                        /// خطا در لغو عملیات
                        MessageError = "Error in the cancel function. ";
                    }

                    MesExpErrorForms += _GetMEC_Forms + "2300: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //----------
        #endregion RunCancleFunction

        //------------
        ////----------////---------------------------------------------------------------------// Begin Form_Load
        //------------

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                ResetFormMain();

                Cursor = Cursors.Default;
            }
            catch (Exception exc)
            {
                Cursor = Cursors.Default;

                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// فرآیند بارگیری برنامه با خطا همراه بود
                    /// مجددا تلاش نمایید
                    MessageError = "\nProgram loading process with errors. \n" +
                                   "\nTry again ... \n";

                    MesExpErrorForms += _GetMEC_Forms + "0000: " + MessageError;
                }
            }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        //------------
        ////----------////---------------------------------------------------------------------// End Form_Load
        //------------
    }
}
