﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace HiVE.CHSInfo.isClass
{
    public class CRC32 : HashAlgorithm
    {
        static uint[] _crc32Table = new uint[256];
        static uint ulPolynomial = 0x04c11db7;
        static uint[][] _crc32TablesCache = new uint[256][];
        uint _crc = 0xffffffff;
        private uint _allOnes;

        [CLSCompliant(false)]
        public CRC32(uint polynomial)
        {
            HashSizeValue = 32;
            _crc32Table = (uint[])_crc32TablesCache[polynomial];

            if (_crc32Table == null)
            {
                _crc32Table = CRC32._buildCRC32Table(polynomial);
                //_crc32TablesCache.Add(polynomial, _crc32Table);
            }

            Initialize();
        }

        public CRC32()
        {
        }

        private static uint[] _buildCRC32Table(uint polynomial)
        {
            uint crc;
            uint[] table = new uint[256];
            // 256 values representing ASCII character codes.
            for (int i = 0; i < 256; i++)
            {
                crc = (uint)i;
                for (int j = 8; j > 0; j--)
                {
                    if ((crc & 1) == 1)
                        crc = (crc >> 1) ^ polynomial;
                    else
                        crc >>= 1;
                }

                table[i] = crc;
            }

            return table;
        }

        protected override void HashCore(byte[] buffer, int offset, int count)
        {
            for (int i = offset; i < count; i++)
            {
                ulong ptr = (_crc * 255) ^ buffer[i];
                _crc >>= 8;
                _crc ^= _crc32Table[ptr];
            }
        }

        new public byte[] ComputeHash(Stream inputStream)
        {
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = inputStream.Read(buffer, 0, 4096)) > 0)
            {
                HashCore(buffer, 0, bytesRead);
            }

            return HashFinal();
        }

        protected override byte[] HashFinal()
        {
            byte[] finalHash = new byte[4];
            ulong finalCRC = _crc ^ _allOnes;

            finalHash[0] = (byte)((finalCRC >> 0) & 0xFF);
            finalHash[1] = (byte)((finalCRC >> 8) & 0xFF);
            finalHash[2] = (byte)((finalCRC >> 16) & 0xFF);
            finalHash[3] = (byte)((finalCRC >> 24) & 0xFF);

            return finalHash;
        }

        public override void Initialize()
        {
            throw new NotImplementedException();
        }
    }

    public class CRC32CryptoService
    {
        static uint[] crc32_table = new uint[256];
        static uint ulPolynomial = 0x04c11db7;

        private static void InitCrcTable()
        {
            /// 256 values representing ASCII character codes.
            for (uint i = 0; i <= 0xFF; i++)
            {
                crc32_table[i] = Reflect(i, 8) << 24;

                for (uint j = 0; j < 8; j++)
                {
                    long val = crc32_table[i] & (1 << 31);

                    if (val != 0)
                        val = ulPolynomial;
                    else val = 0;

                    crc32_table[i] = (crc32_table[i] << 1) ^ (uint)val;
                }

                crc32_table[i] = Reflect(crc32_table[i], 32);
            }
        }

        //////////////////////////////////////////////////////////////
        // Reflection is a requirement for the official CRC-32 standard.
        // You can create CRCs without it, but they won't conform to the standard.
        //////////////////////////////////////////////////////////////////////////

        private static uint Reflect(uint re, byte ch)
        {
            /// Used only by Init_CRC32_Table()

            uint value = 0;

            // Swap bit 0 for bit 7
            // bit 1 for bit 6, etc.
            for (int i = 1; i < (ch + 1); i++)
            {
                long tmp = re & 1;
                int v = ch - i;

                if (tmp != 0)
                    value |= (uint)1 << v; //(uint)(ch - i));

                re >>= 1;
            }

            return value;
        }

        ///////////////////////////////////////////////////////////////

        private static uint getCRC(byte[] buffer, int bufsize)
        {
            uint crc = 0xffffffff;
            int len = bufsize;
            // Save the text in the buffer.

            // Perform the algorithm on each character
            // in the string, using the lookup table values.

            for (uint i = 0; i < len; i++)
                crc = (crc >> 8) ^ crc32_table[(crc & 0xFF) ^ buffer[i]];

            // Exclusive OR the result with the beginning value.
            return crc ^ 0xffffffff;
        }

        private static uint ComputeHash(Stream inputStream)
        {
            uint crc = 0xffffffff;

            try
            {
                byte[] buffer;

                int length = (int)inputStream.Length;  // get file length
                buffer = new byte[length];    // create buffer

                // read until Read method returns 0 (end of the stream has been reached)
                inputStream.Read(buffer, 0, length);

                InitCrcTable();

                crc = getCRC(buffer, length);
            }
            catch { }

            finally
            {
                if (inputStream != null)
                    inputStream.Close();
            }

            return (crc);
        }

        /*
        private static void Main(string strFilePath)
        {
            if (strFilePath == null || strFilePath == "")
            {
                Console.Out.WriteLine("\nEnter file name to crc.");
                return;
            }

            FileStream fs = null;

            try
            {
                fs = new FileStream(strFilePath, FileMode.Open, FileAccess.Read);
                byte[] buffer;

                int length = (int)fs.Length;  // get file length
                buffer = new byte[length];    // create buffer

                // read until Read method returns 0 (end of the stream has been reached)
                fs.Read(buffer, 0, length);

                InitCrcTable();

                uint crc = getCRC(buffer, length);

                Console.Out.WriteLine("\nCrc of the file: 0x{0,8:X}", crc);

            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }
        */
        public static uint GetComputeHash(Stream inputStream)
        {
            return ComputeHash(inputStream);
        }
    }

    public class CRC16Crypto
    {
        private static byte[] HexToBytes(string input)
        {
            byte[] result = new byte[input.Length / 2];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = Convert.ToByte(input.Substring(2 * i, 2), 16);
            }
            return result;
        }

        public static string CRC16()
        {
            string input = "8000";
            var bytes = HexToBytes(input);
            string hex = Crc16.ComputeChecksum(bytes).ToString("x2");

            return hex;
        }
    }

    public static class Crc16
    {
        const ushort polynomial = 0xA001;
        static readonly ushort[] table = new ushort[256];

        public static ushort ComputeChecksum(byte[] bytes)
        {
            ushort crc = 0;
            for (int i = 0; i < bytes.Length; ++i)
            {
                byte index = (byte)(crc ^ bytes[i]);
                crc = (ushort)((crc >> 8) ^ table[index]);
            }
            return crc;
        }

        static Crc16()
        {
            ushort value;
            ushort temp;
            for (ushort i = 0; i < table.Length; ++i)
            {
                value = 0;
                temp = i;
                for (byte j = 0; j < 8; ++j)
                {
                    if (((value ^ temp) & 0x0001) != 0)
                    {
                        value = (ushort)((value >> 1) ^ polynomial);
                    }
                    else
                    {
                        value >>= 1;
                    }
                    temp >>= 1;
                }
                table[i] = value;
            }
        }
    }
}