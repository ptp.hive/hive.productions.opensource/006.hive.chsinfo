﻿using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HiVE.CHSInfo.isClass
{
    public class HashGenerator
    {
        #region Calculate Hash Code Checksum Methods
        //----------

        /// <summary>
        /// Calculate CRC32 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string CRC32Checksum(string strFilePath)
        {
            try
            {
                string strReport = "Program No Complete For CRC!";

                return strReport;

                CRC32 crc32 = new CRC32();
                string hash = string.Empty;
                string strCRC32 = string.Empty;

                //using (FileStream fs = File.Open(strFilePath,FileMode.Open))
                //{
                //    foreach (byte b in crc32.ComputeHash(fs))
                //    {
                //        hash += b.ToString("x2").ToLower();
                //    }
                //    strCRC32 = string.Format("CRC-32 is {0}", hash);
                //}

                uint arrHash_2 = CRC32CryptoService.GetComputeHash(File.OpenRead(strFilePath));

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the MD5 hash code in byte array.
                byte[] arrHash = crc32.ComputeHash(File.OpenRead(strFilePath));

                strCRC32 = arrHash.ToString();

                /// Use a Stringbuilder to append the bytes from the array to create a MD5 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal MD5  hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate CRC64 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string CRC64Checksum(string strFilePath)
        {
            try
            {
                string strReport = "Program No Complete For CRC!";

                return strReport;

                MD5 md5 = new MD5CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the MD5 hash code in byte array.
                byte[] arrHash = md5.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a MD5 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal MD5  hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate MD5 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string MD5Checksum(string strFilePath)
        {
            try
            {
                MD5 md5 = new MD5CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the MD5 hash code in byte array.
                byte[] arrHash = md5.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a MD5 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal MD5  hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate SHA1‐256 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string SHA1Checksum(string strFilePath)
        {
            try
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the SHA1‐256 hash code in byte array.
                byte[] arrHash = sha1.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a SHA1‐256 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal SHA1‐256 hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate SHA2‐256 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string SHA256Checksum(string strFilePath)
        {
            try
            {
                SHA256 sha256 = new SHA256CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the SHA2‐256 hash code in byte array.
                byte[] arrHash = sha256.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a SHA2‐256 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal SHA2‐256 hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate SHA2‐384 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string SHA384Checksum(string strFilePath)
        {
            try
            {
                SHA384 sha384 = new SHA384CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the SHA2‐384 hash code in byte array.
                byte[] arrHash = sha384.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a SHA2‐384 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal SHA2‐384 hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        /// <summary>
        /// Calculate SHA2‐512 Hash Code Checksum Method
        /// </summary>
        /// <param name="strFilePath"></param>
        /// <returns></returns>
        private static string SHA512Checksum(string strFilePath)
        {
            try
            {
                SHA512 sha512 = new SHA512CryptoServiceProvider();

                /// Provide the string in byte format to the ComputeHash method.
                /// This method returns the SHA2‐512 hash code in byte array.
                byte[] arrHash = sha512.ComputeHash(File.OpenRead(strFilePath));

                /// Use a Stringbuilder to append the bytes from the array to create a SHA2‐512 hash code string.
                StringBuilder sbHash = new StringBuilder();

                /// Loop through byte array of the hashed code and format each byte as a hexadecimal code.
                for (int i = 0; i < arrHash.Length; i++)
                {
                    sbHash.Append(arrHash[i].ToString("x2"));
                }

                /// Return the hexadecimal SHA2‐512 hash code string.
                return sbHash.ToString().ToUpper();
            }
            catch { return null; }
        }

        //----------
        #endregion Calculate Hash Code Checksum Methods

        //----------

        #region Get Calculate Hash Code Checksum Methods
        //----------

        /// <summary>
        /// CRC32 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetCRC32Checksum(string _strFilePath)
        {
            return CRC32Checksum(_strFilePath);
        }

        /// <summary>
        /// CRC64 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetCRC64Checksum(string _strFilePath)
        {
            return CRC64Checksum(_strFilePath);
        }

        /// <summary>
        /// MD5 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetMD5Checksum(string _strFilePath)
        {
            return MD5Checksum(_strFilePath);
        }

        /// <summary>
        /// SHA1‐256 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetSHA1Checksum(string _strFilePath)
        {
            return SHA1Checksum(_strFilePath);
        }

        /// <summary>
        /// SHA2‐256 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetSHA256Checksum(string _strFilePath)
        {
            return SHA256Checksum(_strFilePath);
        }

        /// <summary>
        /// SHA2‐384 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetSHA384Checksum(string _strFilePath)
        {
            return SHA384Checksum(_strFilePath);
        }

        /// <summary>
        /// SHA2‐512 Get Calculate Hash Code Checksum Method
        /// </summary>
        /// <param name="_strFilePath"></param>
        /// <returns></returns>
        public static string GetSHA512Checksum(string _strFilePath)
        {
            return SHA512Checksum(_strFilePath);
        }

        //----------
        #endregion Calculate Hash Code Checksum Methods
    }
}
