﻿using System.Windows.Forms;

namespace HiVE.CHSInfo.isClass
{
    public class TryCatch
    {
        #region TryCatch Parametrs and Values
        //----------

        /// <summary>
        /// Algoritm ShowMessageError
        /// </summary>
        /// <returns>MessageError = "t-ab-cd-e-f"</returns>
        private static string Algoritm_ShowMessageError()
        {
            string MessageError = "t-ab-cd-e-f";

            /// 't' is for type [Forms/ Models] Program
            ///     1 = isForm
            ///     2 = isClass

            /// 'ab' is for specification Forms Program
            ///     02 = FormStartup
            ///     03 = FormLogin
            ///     04 = FormMain

            /// 'ab' is for specification Models Program
            ///     11 = Backup_Restore

            /// 'cd' is for Agency - Number[errors] each Function


            /// 'e' is for specification Querys:
            ///     0 = noQuery
            ///     1 = SELECT
            ///     2 = INSERT
            ///     3 = UPDATE
            ///     4 = DELETE

            /// 'f' is for Agency - Number errors Sections - Function
            ///     0 = is first Section

            return MessageError;
        }

        /// <summary>
        /// Message Error Code Static
        /// </summary>
        private bool ShowFriendlyMessage = true;

        /// <summary>
        /// خطا در برقراری ارتباط با پایگاه‌داده
        /// </summary>
        private static string MEC_ConnectionDB = "Error communicating with the database. ";

        /// <summary>
        /// خطای
        /// </summary>
        private static string MesErrCode_Error = "\nError ";

        /// <summary>
        /// Type is Form
        /// </summary>
        private static string MesErrType_isForm = "1";

        /// <summary>
        /// Type is Class-Model
        /// </summary>
        private static string MesErrType_isClass = "2";

        /// <summary>
        /// Message Error Code Main Forms
        /// </summary>
        private static string MEC_LoadProgram = "01";
        private static string MEC_FormStartup = "02";
        private static string MEC_FormLogin = "03";
        private static string MEC_FormMain = "04";
        private static string MEC_FormAbout = "05";

        /// <summary>
        /// Message Error Code Other Forms
        /// </summary>


        /// <summary>
        /// Message Error Code Models
        /// </summary>
        private static string MEC_McsHashGenerator = "11";
        private static string MEC_McsCRCHashAlgorithm = "12";

        //----------

        private bool CatchExceptionMesError(string MessageError)
        {
            bool isError = false;

            try
            {
                if (MessageError != "")
                {
                    isError = true;

                    MessageBox.Show(
                        MessageError,
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);

                    MessageError = "";
                }
            }
            catch { }

            return isError;
        }

        //----------
        #endregion TryCatch Parametrs and Values

        //------------

        #region TryCatch Base GetCEM_Error [GetCatchExceptionMesError]
        //----------

        public bool GetShowFriendlyMessage
        {
            get { return ShowFriendlyMessage; }
        }

        public bool GetCEM_Error(string MessageError)
        {
            return CatchExceptionMesError(MessageError);
        }

        public string GetMEC_ConDB
        {
            get { return MEC_ConnectionDB; }
        }

        //----------
        #endregion TryCatch Base GetCEM_Error [GetCatchExceptionMesError]

        #region TryCatch Message Error Code Main Forms
        //----------

        public string GetMEC_LoadProgram
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isForm +
                       MEC_LoadProgram;
            }
        }

        public string GetMEC_FormStartup
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isForm +
                       MEC_FormStartup;
            }
        }

        public string GetMEC_FormLogin
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isForm +
                       MEC_FormLogin;
            }
        }

        public string GetMEC_FormMain
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isForm +
                       MEC_FormMain;
            }
        }

        public string GetMEC_FormAbout
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isForm +
                       MEC_FormAbout;
            }
        }

        //----------
        #endregion TryCatch Message Error Code Main Forms

        #region TryCatch Message Error Code Main Forms
        //----------

        //----------
        #endregion TryCatch Message Error Code Main Forms

        #region TryCatch Message Error Code Models
        //----------

        public string GetMEC_McsHashGenerator
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isClass +
                       MEC_McsHashGenerator;
            }
        }

        public string GetMEC_McsCRCHashAlgorithm
        {
            get
            {
                return MesErrCode_Error +
                       MesErrType_isClass +
                       MEC_McsCRCHashAlgorithm;
            }
        }

        //----------
        #endregion TryCatch Message Error Code Models

    }
}