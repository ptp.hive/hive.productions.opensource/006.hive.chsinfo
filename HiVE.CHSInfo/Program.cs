﻿using HiVE.CHSInfo.isForm;
using System;
using System.Windows.Forms;

namespace HiVE.CHSInfo
{
    static class Program
    {
        #region ConnectionTryCatch
        //----------

        private static bool GetShowFriendlyMessage = true;
        private static string _GetMEC_Forms = string.Empty;
        private static string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionTryCatch

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                #region Load Application
                //----------

                using (frmStartup _frmStartup = new frmStartup())
                {
                    if (_frmStartup.ShowDialog() != DialogResult.OK) return;
                }

                Application.Run(new frmMain());

                //----------
                #endregion Load Application                
            }
            catch (Exception exc)
            {
                _GetMEC_Forms = "-2";
                MesExpErrorForms = string.Empty;

                if (!GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _GetMEC_Forms + "0000: " + exc.Message;
                }
                else
                {
                    string MessageError = string.Empty;

                    /// فرآیند بارگیری برنامه با خطا همراه بود
                    /// مجددا تلاش نمایید
                    MessageError = "\nProgram loading process with errors. \n" +
                                   "\nTry again ... \n";

                    MesExpErrorForms += _GetMEC_Forms + "0000: " + MessageError;
                }

                MessageBox.Show(
                        MesExpErrorForms,
                        "Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1);
            }
        }
    }
}
